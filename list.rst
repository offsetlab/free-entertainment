- Books

  - `Project Gutenberg <http://www.gutenberg.org/wiki/Main_Page>`_

- Music

  - `A whole lot of internet radio stations <https://en.wikipedia.org/wiki/List_of_Internet_radio_stations#Internet-only>`_
  - `Jamendo <https://www.jamendo.com/en>`_
  - `Magnatune <https://magnatune.com/>`_
  - `Bandcamp <https://bandcamp.com/>`_
  - `Soundcloud <https://soundcloud.com/>`_

- Visual

  - `GoogleArt <http://www.googleartproject.com/>`_
  - `WikiArt <http://http://www.wikiart.org/>`_

- Games

  - `Abandonia <http://www.abandonia.com/>`_

- Culture

  - `Europeana <http://www.europeana.eu/portal/>`_

